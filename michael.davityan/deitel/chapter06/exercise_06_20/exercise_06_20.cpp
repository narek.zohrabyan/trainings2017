#include <iostream>
#include <cassert>
#include <string>

bool multiple(const int firstNumber, const int secondNumber);

int
main() 
{
    int firstNumber;
    std::cin >> firstNumber;
    if (0 == firstNumber) {
        std::cout << "Error 1: division by zero." << std::endl;
        return 1;
    } 
    int secondNumber;
    std::cin >> secondNumber;
    std::cout << multiple(firstNumber, secondNumber) << std::endl;
    
    return 0;
}

bool
multiple(const int firstNumber, const int secondNumber)
{
    assert(firstNumber != 0);
    if (0 == secondNumber % firstNumber) {
        return true;
    }
    return false;
}
