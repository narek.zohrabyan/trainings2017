#include <iostream>
#include <cassert>

int 
integerPower(const int base, int exponent)
{ 
    assert(exponent > 0);

    int value = 1;
    while (exponent > 0) {
        value *= base;
        --exponent;
    }
    return value;
}

int
main()
{
    int base;
    std::cout << "Enter base: ";
    std::cin  >> base;

    int exponent;
    std::cout << "Enter exponent: ";
    std::cin  >> exponent;

    if (exponent < 1) {
        std::cerr << "Error 1: Exponent can note be negative." << std::endl;
        return 1;
    }

    std::cout << integerPower(base, exponent) << std::endl;
    return 0;
}
