///Mikayel Ghaziyan
///Exercise 2.7
///09/10/2017

a. std::cout

When std::out is executed, it sends the stream of characters to the screen. The cout is a part of the std namespace whcih controls the streams of characters in and out.Thus, the std::cout is a standard output stream object. The double colon notation (::) is requred by the preprocessor directive #include <iostream> which shows that the cout belongs to the namespace std.

b.std::cin

When the std::in is executed, it reads the stream of characters from the screen.Like cout, the cin is also a part of the std namespace. The std::cin uses the input stream object of the std namespace to obtain data from the screen (user). The std::cin is capable of obtaining both integers and stream characters.

The std::cout and the std::cin stream objects faciliate interaction between the user and the computer which is called an interactive computing.

