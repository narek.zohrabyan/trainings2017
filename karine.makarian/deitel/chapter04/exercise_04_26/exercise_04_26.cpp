#include <iostream>
    
int
main()
{
    int number;  

    std::cout << "Enter a 5 digit number: ";
    std::cin >> number;

    int copyNumber = number;
    int reversed = 0;
    
    while (copyNumber != 0) {
        int digit = number % 10;
        reversed = (reversed * 10) + digit;
        copyNumber /= 10;
    }
   
    if (number == reversed) {
        std::cout << "The number is palindrome." << std::endl;
        return 0;
    }
   
    std::cout << "The number is not palindrome." << std::endl;
    
    return 0;
}

