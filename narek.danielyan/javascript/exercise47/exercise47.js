$(document).ready(function () {
    "use strict";
    var obj = {
        className: 'open menu'
    };
    function addClass(obj, cls) {
        var classes = obj.className.split(" ");
        if (classes.indexOf(cls) === -1) {
            classes.push(cls);
        }
        obj.className = classes.join(" ");
        return obj.className;
    }
    console.log(addClass(obj, "new"));
});
