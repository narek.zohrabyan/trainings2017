#include <iostream>

int
main()
{
    float sum = 0;
    int counter = 0;

    while (true) {
        float number;

        std::cout << "Please enter a number (9999 if you finished): ";
        std::cin >> number;

        if (9999 == number) {
            break;
        }

        sum += number;
        ++counter;
    }

    if (0 == counter) {
        std::cerr << "Error 1: You entered nothing." << std::endl;
        return 1;
    }

    float average = sum / counter;
    
    std::cout << "Average: " << average << std::endl;

    return 0;
}
