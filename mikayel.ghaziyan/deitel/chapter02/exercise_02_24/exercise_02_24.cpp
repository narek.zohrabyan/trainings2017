/// Mikayel Ghaziyan
/// 16/10/2017
/// Exercise 2.24

#include <iostream>

/// Beginning of the main
int
main()
{
    int number;

    /// Input data
    std::cout << "Please enter a whole number: ";
    std::cin >> number;

    /// Evaluation
    if (number % 2 == 0) {
        std::cout << "The number is even." << std::endl;
 
	return 0;
    }

    std::cout << "The number is odd." << std::endl;

    return 0;
} /// End of the main

/// End of the file

