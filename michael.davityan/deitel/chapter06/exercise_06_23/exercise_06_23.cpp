#include <iostream>
#include <cassert>
#include <string>

void square(int side, char fillCharacter);

int
main()
{
    int side;
    std::cin >> side;
    if (side < 0) {
        std::cout << "Error 1: wrong side." << std::endl;
        return 1;
    }

    char fillCharacter;
    std::cin >> fillCharacter;
    square(side, fillCharacter); 

    return 0;
}

void
square(int side, char fillCharacter)
{
    assert(side >= 0);
    for (int row = 1; row <= side; ++row) {
        for (int column = 1; column <= side; ++column) {
            std::cout << fillCharacter << " ";
        }
        std::cout << std::endl;
    }
}
