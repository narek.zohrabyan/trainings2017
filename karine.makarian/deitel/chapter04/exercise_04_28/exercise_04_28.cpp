#include <iostream>

int
main()
{
    int count1 = 1;

    while (count1 <= 8) { 
        int count2 = 1;

        if (0 == count1 % 2) { 
            std::cout << " ";
        }

        while (count2 <= 8) {    
            std::cout << "* ";
            ++count2;
        }
                                                
        std::cout << std::endl;
        ++count1;
    }
    return 0;
}

