Question...
State whether the following are true or false. If false, explain why.
a. Two pointers that point to different arrays cannot be compared meaningfully.
b. Because the name of an array is a pointer to the first element of the array, array names can be 
manipulated in precisely the same manner as pointers.

Answer....
a.)Its true because the adresses is different.For example first massive adress can be 3000, and the 
second can be 5000. Two pointers must point to the same massive, for example first pointer can point to the massive name with 3000 adress and the second can point to the massive second element with 3008 
adress.

b.)Its false... There is only one think that we cant use array name as the same with pointer...
we cant increment array name or modify it because it is constant pointer, thats always point 
to begin of array.



