Question. Identify and correct the error(s) in each of the following:
a. if ( age >= 65 );
   cout << "Age is greater than or equal to 65" << endl;
   else
   cout << "Age is less than 65 << endl";

b. if ( age >= 65 )
   cout << "Age is greater than or equal to 65" << endl;
   else;
   cout << "Age is less than 65 << endl";

c. int x = 1, total;
   while ( x <= 10 )
   {
   total += x;
   x++;
   }

d. while ( x <= 100 )
   total += x;
   x++;

e. while ( y > 0 )
   {
   cout << y << endl;
   y++;
   }

Answer. 

a. if ( age >= 65 ) /// ;
   cout << "Age is greater than or equal to 65" << endl;
   else
   cout << "Age is less than 65" << endl; /// " - this at the endl.

b. if ( age >= 65 )
   cout << "Age is greater than or equal to 65" << endl;
   else /// ;
   cout << "Age is less than 65" << endl; /// " - this at the endl

c. int x = 1, total = 0; /// total not initialized by 0;
   while ( x <= 10 )
   {
   total += x;
   x++;
   }

d. int x = 1, total = 0; /// initializations are missed.
   while ( x <= 100 ) /// while have more than one statement in its body brackets arent used... 
   {
   total += x;
   x++;
   }

e. int y = 10; /// initialization are missed...
   while ( y > 0 ) 
   { 
   cout << y << endl;
   y--; /// with increase while falls into an endless cycle. I think decrease.
   }

