a.
if(age >= 65) {
    std::cout << "Age is greater than or equal to 65" << std::endl;
}
else {
    std::cout << "Age is less than 65 << std::endl"; ///must be "Age is less than or equal to 65" << endl;
}

b. 
if(age >= 65) {
    std::cout << "Age is greater than or equal to 65" << std::endl;
}
else;  ///must be w/o ; {
    std::cout << "Age is less than 65 << std::endl"; ///must be "Age is less  65" << endl;
}

c. 
int x = 1, total; ///from next line must be written  int total = 0;
while(x <= 10)
{
    total += x;
    x++;
}

d. 
While(x <= 100) ///must be {} otherwise while's body includes only 1 command
total += x;              { 
x++;                         total += x;
                             x++;
                         }

e. 
while(y > 0)
{
    std::cout << y << std::endl;   ///endless while...must be y--
    y++;
}

