#include <iostream>
#include <unistd.h>

void bubbleSort(int array[], const int arraySize);

int
main()
{
    const int ARRAY_SIZE = 10;
    int array[ARRAY_SIZE];

    if (::isatty(STDIN_FILENO)) {
        std::cout << "This program shows how the bubble sort works\n";
        std::cout << "Insert 10 elements of array\n";
    }
    for (int index = 0; index < ARRAY_SIZE; ++index) {
        std::cin >> array[index];
    }
    std::cout << "array data in the original order.\n";
    for (int index = 0; index < ARRAY_SIZE; ++index) {
        std::cout << array[index] << " ";
    }
    bubbleSort(array, ARRAY_SIZE);
    std::cout << "\narray data in ascending order.\n";
    for (int index = 0; index < ARRAY_SIZE; ++index) {
        std::cout << array[index] << " ";
    }
    std::cout << std::endl;

    return 0;
}

void 
bubbleSort(int array[], const int arraySize)
{
    for (int pass = 0; pass < arraySize - 1; ++pass) {
        int hold;
        for (int index = 0; index < arraySize - 1; ++index) {
            if (array[index] > array[index + 1]) {
                hold = array[index];
                array[index] = array[index + 1];
                array[index + 1] = hold;
            }
        }       
    }
}
