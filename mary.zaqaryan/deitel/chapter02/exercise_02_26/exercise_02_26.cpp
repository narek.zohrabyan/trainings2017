#include <iostream>

int
main()
{
    ///first version of outputting with eight output statements

    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n\n";

    ///second version with one statement
    std::cout << "* * * * * * * *\n"
                 " * * * * * * * *\n"
                 "* * * * * * * *\n"
                 " * * * * * * * *\n"
                 "* * * * * * * *\n"
                 " * * * * * * * *\n"
                 "* * * * * * * *\n"
                 " * * * * * * * *\n";

    return 0;
};
