#include <iostream>
#include <cassert>

double
integerPower(int base, int exponent)
{
    assert(exponent > 0);
    double result = 1.0;
    while (exponent != 0) {
        if ((exponent & 1) != 0) {
            result *= base;
            --exponent;
        }
        base *= base;
        exponent /= 2;
    }
    return result;
}

int
main()
{
    int number1;
    int number2;

    std::cout << "Enter a pair of numbers(base, exponent): ";
    std::cin >> number1 >> number2;

    if (number2 <= 0) {
        std::cerr << "Error 1: Non positive integer was inputed." << std::endl;
        return 1;
    }

    std::cout << "Result is: " << integerPower(number1, number2) << std::endl;

    return 0;
}

