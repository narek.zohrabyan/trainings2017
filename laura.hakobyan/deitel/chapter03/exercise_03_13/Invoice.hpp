#include <string>

class Invoice
{
public: 
    Invoice(std::string partNumber, std::string partDescription, int purchaseQuantity, int pricePerItem);
    void setPartNumber(std::string partNumber);
    std::string getPartNumber();
    void setPartDescription(std::string partDescription);
    std::string getPartDescription();
    void setPurchaseQuantity(int purchaseQuantity);
    int getPurchaseQuantity();
    void setPricePerItem(int pricePerItem);
    int getPricePerItem();
    int getInvoiceAmount();
private:
    std::string partNumber_;
    std::string partDescription_;
    int purchaseQuantity_;
    int pricePerItem_;
};

