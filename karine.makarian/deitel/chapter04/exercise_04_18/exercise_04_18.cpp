#include <iostream>
    
int
main()
{        
    int number = 1;

    std::cout << "N\t" << "N * 10\t" << "N * 100\t" << "N * 1000" << std::endl;

    while (number < 6) {
        std::cout << number << '\t' << number * 10 << '\t' << number * 100 << '\t' << number * 1000 << std::endl;
        ++number;
    }

    return 0;
}
