#include <iostream>

int
main() 
{ 
    int integer1, integer2;
    std::cout << "Enter two integers: ";   
    std::cin >> integer1 >> integer2;

    if (integer1 > integer2) {
         std::cout << integer1  << " is larger." << std::endl;
         return 0;   
    }
    if (integer2 > integer1) {
        std::cout << integer2 << " is larger." << std::endl;
        return 0;    
    } 
    std::cout << "These numbers are equal." << std::endl;
    return 0;
} 

