$(document).ready(function () {
    "use strict";
    function sum() {
        var total = 0, i = 0;
        for (i = 0; i < arguments.length; ++i) {
            total += arguments[i];
        }
        return total;
    }
    console.log(sum(1, 2, 3));
});
