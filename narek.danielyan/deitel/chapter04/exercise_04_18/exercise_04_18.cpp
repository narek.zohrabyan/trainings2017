#include<iostream>

int
main()
{
    int i = 1;

    std::cout << "N\t" << "10*N\t" << "100*N\t" << "1000*N\t" << "\n\n";

    while (i <= 5) {
        std::cout << i << "\t" << 10 * i << "\t" << 100 * i << "\t" << 1000 * i << std::endl;
        i++;
    }
    return 0;
}
