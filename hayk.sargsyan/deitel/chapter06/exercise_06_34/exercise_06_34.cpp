#include <iostream>
#include <cstdlib>

bool 
flip()
{
    return static_cast<bool>(std::rand() % 2);
}

int
main()
{
    int tailsCounter = 0;
    int eagleCounter = 0;
    
    for (int counter = 1; counter <= 100; ++counter) {
        if (flip()) {
            std::cout << "tails" << std::endl; 
            ++tailsCounter;
        } else {
            std::cout << "eagle" << std::endl; 
            ++eagleCounter;
        }
    }

    std::cout << "flipped tails " << tailsCounter << " times" << std::endl
              << "flipped eagle " << eagleCounter << " times" << std::endl;

    return 0;
}
