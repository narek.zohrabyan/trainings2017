#include <iostream>
#include <unistd.h>

template <typename T>
T 
max(const T variable1, const T variable2) 
{
    return variable1 >= variable2 ? variable1 : variable2;
}

int 
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This part of program defines the maximum from two integers.\n";
        std::cout << "Insert first  integer: ";
    }
    int number1;
    std::cin >> number1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert second integer: ";
    }
    int number2;
    std::cin >> number2;
    std::cout << "The maximum from two is " << max(number1, number2) 
              << std::endl;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "This part of program defines the maximum from two floating point numbers.\n";
        std::cout << "Insert first  floating point number: ";
    }
    double number3;
    std::cin >> number3;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert second floating point number: ";
    }
    double number4;
    std::cin >> number4;
    std::cout << "The maximum from two is " << max(number3, number4) 
              << std::endl;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "This part of program defines the maximum from two characters.\n";
        std::cout << "Insert first  character: ";
    }
    char character1;
    std::cin >> character1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert second character: ";
    }
    char character2;
    std::cin >> character2;
    std::cout << "The maximum from two is " << max(character1, character2) 
              << std::endl;

    return 0;
}

