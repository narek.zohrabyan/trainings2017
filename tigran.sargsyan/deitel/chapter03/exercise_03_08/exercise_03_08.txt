Source code contains of function definitions and main function. Header file usually contains of class definition and function prototypes. It could be included in the source code (it is done for utilization of program components, which could be reused many times, and for separating interface from implementation).

