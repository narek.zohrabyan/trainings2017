#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin >> number;

    if (number < 0) {
        std::cerr << "Error 1. Entered wrong number." << std::endl;
        return 1;
    }
    if (number > 9999) {
        std::cerr << "Error 1. Entered wrong number." << std::endl;
        return 1;
    }

    int digit1 = (number / 1000 + 3) % 10;
    int digit2 = (number / 100 + 3) % 10;
    int digit3 = (number / 10 + 3) % 10;
    int digit4 = (number + 3) % 10;
    int newNumber = 1000 * digit3 + 100 * digit4 + 10 * digit1 + digit2;

    std::cout << "Encrypted number is " << newNumber << std::endl;
    return 0;
}

