#include <iostream>
#include <unistd.h>
#include <cassert>

void bubbleSort(int array[], const size_t arraySize);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert 10 elements of array.\n";
    }
    const size_t ARRAY_SIZE = 10;
    int array[ARRAY_SIZE];
    for (size_t index = 0; index < ARRAY_SIZE; ++index) {
        std::cin >> array[index];
    }
    std::cout << "array in the initial state.\n"; 
    for (size_t index = 0; index < ARRAY_SIZE; ++index) {
        std::cout << array[index] << " ";
    }
    bubbleSort(array, ARRAY_SIZE);
    std::cout << "\narray after sorting.\n"; 
    for (size_t index = 0; index < ARRAY_SIZE; ++index) {
        std::cout << array[index] << " ";
    }
    std::cout << std::endl;

    return 0;
}

void 
bubbleSort(int array[], const size_t arraySize) 
{
    assert(arraySize > 0);
    size_t passQuantity = arraySize - 1;
    size_t numberOfComparisons = arraySize - 1;
    for (size_t pass = 0; pass < passQuantity; ++pass) {
        bool passState = false;
        for (size_t index = 0; index < numberOfComparisons; ++index) {
            if (array[index] > array[index + 1]) {
                passState = true;
                int hold = array[index];
                array[index] = array[index + 1];
                array[index + 1] = hold;
            }
        }
        if (!passState) {
            break;
        }
        --numberOfComparisons;
    }     
}
