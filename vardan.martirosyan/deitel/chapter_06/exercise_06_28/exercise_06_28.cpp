#include <iostream>

double minimum(const double firstNumber, const double secondNumber, const double thirdNumber);

int
main()
{
    double firstNumber;
    std::cout << "Enter first number: ";
    std::cin  >> firstNumber;
    double secondNumber;
    std::cout << "Enter second number: ";
    std::cin  >> secondNumber;
    double thirdNumber;
    std::cout << "Enter third number: ";
    std::cin  >> thirdNumber;

    std::cout << "Minimum number is " << minimum(firstNumber, secondNumber, thirdNumber) << std::endl;
    
    return 0;
}

double
minimum(const double firstNumber, const double secondNumber, const double thirdNumber)
{
    double min = firstNumber;
    if (secondNumber <= min) {
        min = secondNumber;
    }
    if (thirdNumber <= min) {
        min = thirdNumber;
    }
    return min;
}


